#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 14 09:17:14 2019

@author: khitishmohanty
"""

import argparse
from pyspark.ml.feature import IDF
from pyspark.ml.classification import LogisticRegression
from pyspark import sql
from pyspark.sql import functions as F
from pyspark.sql.types import IntegerType
from pyspark.ml import Pipeline
from pyspark.ml.feature import RegexTokenizer, CountVectorizer
import Library.config
import pandas as pd


def train_test_split(dataset):    
    #randomly split the dataset to traning and testing 80%, 20% respectively
    (trainingData, testData) = dataset.randomSplit([0.8, 0.2], seed = 100)
    return trainingData, testData

def model_logistic_regression(sampled, trainingData, testData, pipelineFit):
    for i in range(1,6):
        dataset = pipelineFit.transform(sampled)
        lr = LogisticRegression(maxIter=10000, regParam=0.3, elasticNetParam=0, family = "binomial")
        # Train model using logisitic regression
        lrModel = lr.fit(trainingData)
        predictions = lrModel.transform(testData)
    
    #saving the model
    #lrModel.save("LogisticRegression_Model")
    
    df = predictions.select('prediction', 'label')
    
    tp = df[(df.label == 1) & (df.prediction == 1)].count()
    tn = df[(df.label == 0) & (df.prediction == 0)].count()
    fp = df[(df.label == 0) & (df.prediction == 1)].count()
    fn = df[(df.label == 1) & (df.prediction == 0)].count()
    
    r = float(tp)/(tp + fn)
    p = float(tp) / (tp + fp)
    a = float(tp + tn) / (tp + fp + tn + fn)
    f1 = float(p*r)/(p+r) * 2
        
    return tp, tn, fp, fn, r, p, a, f1, lrModel, predictions


    

def read_data(spark):
    data_df = spark.read.csv(path=Library.config.inputDataset, sep=',', encoding='UTF-8', comment=None, header=True,  inferSchema=True)
    
    data_df = data_df[['host', 'class']]
    
    col_update_func = (F.when(F.col('class') == "legit", 0).otherwise(F.col('class')))
    data_df = data_df.withColumn('class', col_update_func)
    
    col_update_func = (F.when(F.col('class') == "dga", 1).otherwise(F.col('class')))
    data_df = data_df.withColumn('class', col_update_func)
    
    data_df = data_df.withColumnRenamed("host","url").withColumnRenamed("class","label")
      
    return data_df


def sample_data(data_df):
    dga = data_df.filter("label = 1")
    legit = data_df.filter("label = 0")
    
    sampleRatio = dga.count() / legit.count()
    sample_legit = legit.sample(False, sampleRatio)
    
    sampled = dga.unionAll(sample_legit)
    sampled = sampled.withColumn("label", sampled["label"].cast(IntegerType()))
    
    return sampled

def vectorize_data(sampled):
    
    #Tokennize the TrainData - sparse the URL string into words
    regexTokenizer = RegexTokenizer(inputCol="url", outputCol="Words", pattern="\\W")
    
    #CountVectorizer converts the the words into feature vectors
    countVectors = CountVectorizer(inputCol=regexTokenizer.getOutputCol(), outputCol="rawfeatures", vocabSize=10000, minDF=5)
    
    idf = IDF(inputCol=countVectors.getOutputCol(), outputCol="features") 
    
    #create the pipline 
    pipeline = Pipeline(stages=[regexTokenizer, countVectors, idf])
    
    # Fit the pipeline to training documents.
    # Pass 'sampled' in the param to set Balanced datasets
    pipelineFit = pipeline.fit(sampled)
    
    #Transform the pipeline to dataset
    # Pass 'sampled' in the param to set Balanced datasets
    dataset = pipelineFit.transform(sampled)
    
    return dataset, pipelineFit


def logistic_model():
    spark = sql.SparkSession.builder.appName("Detecting-Malicious-URL App").getOrCreate()
    #Read input data
    data_df = read_data(spark)
    #Data sampling
    sampled = sample_data(data_df)
    #Data Ingestion and Vectorization
    dataset, pipelineFit = vectorize_data(sampled)
    #Splitting of data
    trainingData, testData = train_test_split(dataset)
    #build LOGISTIC REGRESSION
    tp, tn, fp, fn, r, p, a, f1, lrModel, predictions = model_logistic_regression(sampled, trainingData, testData, pipelineFit)
    return lrModel, spark, sampled
###############################################
    

# Create the parser
my_parser = argparse.ArgumentParser(description='DGA URL detection')

# Add the arguments
my_parser.add_argument('url',
                       metavar='url',
                       type=str,
                       help='url for DGA detection')

# Execute the parse_args() method
args = my_parser.parse_args()

test_url = [args.url]

lrModel, spark, sampled = logistic_model()
#Extract test_url features
test_df = pd.DataFrame(columns=['url', 'label'])
test_df['url'] = test_url
test_df['label'] = 0
test_df_SPARK = spark.createDataFrame(test_df)
sampled = sampled.union(test_df_SPARK)
#Vectorisation
dataset, pipelineFit = vectorize_data(sampled)
#select the test data
test_df_SPARK = spark.createDataFrame(dataset.toPandas().tail(1))
lr_predictions = lrModel.transform(test_df_SPARK)
y_pred = lr_predictions.select("prediction").toPandas()

if (y_pred.prediction[0]==1.0):
    print("\n\n\nInput URL: "+ str(test_url[0]) +"\n\nThis URL is generated through DGA\n\n")
else:
    print("\n\n\nInput URL: "+ str(test_url[0]) +"\n\nThis URL doesn't seem to be generated through DGA\n\n")

