#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 10:33:15 2019

@author: khitishmohanty
"""

from pyspark.ml.classification import LogisticRegression
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import numpy as np
from pyspark import sql
from Library.ProcessData import read_data, sample_data, vectorize_data


def train_test_split(dataset):    
    #randomly split the dataset to traning and testing 80%, 20% respectively
    (trainingData, testData) = dataset.randomSplit([0.8, 0.2], seed = 100)
    return trainingData, testData

def model_logistic_regression(sampled, trainingData, testData, pipelineFit):
    for i in range(1,6):
        dataset = pipelineFit.transform(sampled)
        lr = LogisticRegression(maxIter=10000, regParam=0.3, elasticNetParam=0, family = "binomial")
        # Train model using logisitic regression
        lrModel = lr.fit(trainingData)
        predictions = lrModel.transform(testData)
    
    #saving the model
    #lrModel.save("LogisticRegression_Model")
    
    df = predictions.select('prediction', 'label')
    
    tp = df[(df.label == 1) & (df.prediction == 1)].count()
    tn = df[(df.label == 0) & (df.prediction == 0)].count()
    fp = df[(df.label == 0) & (df.prediction == 1)].count()
    fn = df[(df.label == 1) & (df.prediction == 0)].count()
    
    r = float(tp)/(tp + fn)
    p = float(tp) / (tp + fp)
    a = float(tp + tn) / (tp + fp + tn + fn)
    f1 = float(p*r)/(p+r) * 2
        
    return tp, tn, fp, fn, r, p, a, f1, lrModel, predictions


def plot_logistic_regression(testData, lrModel):
    lr_predictions = lrModel.transform(testData)
    
    y_actu = lr_predictions.select("label").toPandas()
    y_pred = lr_predictions.select("prediction").toPandas()
    
    cm = confusion_matrix(y_actu, y_pred)
    
    plt.clf()
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Wistia)
    classNames = ['Negative','Positive']
    plt.title('LOGISTIC REGRESSION')
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    tick_marks = np.arange(len(classNames))
    plt.xticks(tick_marks, classNames, rotation=45)
    plt.yticks(tick_marks, classNames)
    
    s = [['TN','FP'], ['FN', 'TP']]
    for i in range(2):
        for j in range(2):
            plt.text(j,i, str(s[i][j])+" = "+str(cm[i][j]))
    plt.show()
    
    beta = np.sort(lrModel.coefficients)
    plt.plot(beta)
    plt.ylabel('Beta Coefficients')
    plt.show()
    
    # Extract the summary from the returned LogisticRegressionModel instance trained
    trainingSummary = lrModel.summary
    
    #Obtain the objective per iteration
    objectiveHistory = trainingSummary.objectiveHistory
    plt.plot(objectiveHistory)
    plt.ylabel('Objective Function')
    plt.xlabel('Iteration')
    plt.show()
    
    pr = trainingSummary.pr.toPandas()
    plt.plot(pr['recall'],pr['precision'])
    plt.ylabel('Precision')
    plt.xlabel('Recall')
    plt.show()
    
    #Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
    print("areaUnderROC: " + str(trainingSummary.areaUnderROC))
    
    #trainingSummary.roc.show(n=10, truncate=15)
    roc = trainingSummary.roc.toPandas()
    plt.plot(roc['FPR'],roc['TPR'])
    plt.ylabel('False Positive Rate')
    plt.xlabel('True Positive Rate')
    plt.title('ROC Curve')
    plt.show()
    
    #Set the model threshold to maximize F-Measure
    trainingSummary.fMeasureByThreshold.show(n=10, truncate = 15)
    f = trainingSummary.fMeasureByThreshold.toPandas()
    plt.plot(f['threshold'],f['F-Measure'])
    plt.ylabel('F-Measure')
    plt.xlabel('Threshold')
    plt.show()
    
def logistic_model():
    spark = sql.SparkSession.builder.appName("Detecting-Malicious-URL App").getOrCreate()
    #Read input data
    data_df = read_data(spark)
    #Data sampling
    sampled = sample_data(data_df)
    #Data Ingestion and Vectorization
    dataset, pipelineFit = vectorize_data(sampled)
    #Splitting of data
    trainingData, testData = train_test_split(dataset)
    #build LOGISTIC REGRESSION
    tp, tn, fp, fn, r, p, a, f1, lrModel, predictions = model_logistic_regression(sampled, trainingData, testData, pipelineFit)
    return lrModel, spark, sampled