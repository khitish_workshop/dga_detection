#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 09:33:50 2019

@author: khitishmohanty
"""

import Library.config 
from pyspark.sql import functions as F
from pyspark.ml.feature import RegexTokenizer, CountVectorizer
from pyspark.ml.feature import IDF
from pyspark.ml import Pipeline
from pyspark.sql.types import IntegerType

def read_data(spark):
    data_df = spark.read.csv(path=Library.config.inputDataset, sep=',', encoding='UTF-8', comment=None, header=True,  inferSchema=True)
    
    data_df = data_df[['host', 'class']]
    
    col_update_func = (F.when(F.col('class') == "legit", 0).otherwise(F.col('class')))
    data_df = data_df.withColumn('class', col_update_func)
    
    col_update_func = (F.when(F.col('class') == "dga", 1).otherwise(F.col('class')))
    data_df = data_df.withColumn('class', col_update_func)
    
    data_df = data_df.withColumnRenamed("host","url").withColumnRenamed("class","label")
      
    return data_df


def sample_data(data_df):
    dga = data_df.filter("label = 1")
    legit = data_df.filter("label = 0")
    
    sampleRatio = dga.count() / legit.count()
    sample_legit = legit.sample(False, sampleRatio)
    
    sampled = dga.unionAll(sample_legit)
    sampled = sampled.withColumn("label", sampled["label"].cast(IntegerType()))
    
    return sampled

def vectorize_data(sampled):
    
    #Tokennize the TrainData - sparse the URL string into words
    regexTokenizer = RegexTokenizer(inputCol="url", outputCol="Words", pattern="\\W")
    
    #CountVectorizer converts the the words into feature vectors
    countVectors = CountVectorizer(inputCol=regexTokenizer.getOutputCol(), outputCol="rawfeatures", vocabSize=10000, minDF=5)
    
    idf = IDF(inputCol=countVectors.getOutputCol(), outputCol="features") 
    
    #create the pipline 
    pipeline = Pipeline(stages=[regexTokenizer, countVectors, idf])
    
    # Fit the pipeline to training documents.
    # Pass 'sampled' in the param to set Balanced datasets
    pipelineFit = pipeline.fit(sampled)
    
    #Transform the pipeline to dataset
    # Pass 'sampled' in the param to set Balanced datasets
    dataset = pipelineFit.transform(sampled)
    
    return dataset, pipelineFit


