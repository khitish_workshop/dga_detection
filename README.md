# Detecting-Malicious-URL-Using-Pyspark

## ML Model
code documented notebook: DGA_Legit_classification.ipynb

## Development Enviroment
1. Apache Spark 2.4.0
2. Jupyter Notebook
3. Java 8

## Datasets
Datasets used in this project is provided

## TestCase
This includes unit test and integration test cases

## Library
1. BuildModels: ML model creation
2. config: Contains the path of the inout file
3. ProcessData: Data transformation to make it consumable for the ML model


## Command line interface
1. Clone the repo
2. navigate to Library/config.py
3. change the path to the InputDataset folder path

command: python DGA_Legit_classification.py google.com

>Input URL: google.com

>This URL doesn't seem to be generated through DGA

command: python DGA_Legit_classification.py 1008bnt1iekzdt1fqjb76pijxhr.org

>Input URL: 1008bnt1iekzdt1fqjb76pijxhr.org

>This URL is generated through DGA