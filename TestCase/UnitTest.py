#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 22:08:43 2019

@author: khitishmohanty
"""

from Library.BuildModels import logistic_model
from Library.ProcessData import vectorize_data
import pandas as pd

def unit_test_sample(test_url):    
    lrModel, spark, sampled = logistic_model()
    sampled_desc = sampled.groupby('label').count().toPandas()
    sample_ratio = sampled_desc.count[0] / sampled_desc.count[1]
    if (sample_ratio<10):
        return "pass"
    else:
        return "fail"

def unit_test_test_df(): 
    lrModel, spark, sampled = logistic_model()
    #Extract test_url features
    test_df = pd.DataFrame(columns=['url', 'label'])
    test_df['url'] = test_url
    test_df['label'] = 0
    if (len(test_df)==1):
        return "pass"
    else:
        return "fail"

def unit_test_dataset():
    lrModel, spark, sampled = logistic_model()
    test_df = pd.DataFrame(columns=['url', 'label'])
    test_df['url'] = test_url
    test_df_SPARK = spark.createDataFrame(test_df)
    sampled = sampled.union(test_df_SPARK)
    #Vectorisation
    dataset, pipelineFit = vectorize_data(sampled)
    if(sampled.count() == dataset.count()):
        return "pass"
    else:
        return "fail"  


test_url = ["zvwqwnfguqcauhcmzjfskgimndynj.ru"]
unit_test_sample(test_url)
unit_test_test_df()
unit_test_dataset()

    
    
