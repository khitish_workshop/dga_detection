#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 15 17:06:47 2019

@author: khitishmohanty
"""

from Library.BuildModels import logistic_model
from Library.ProcessData import vectorize_data
import pandas as pd

def integration_test(test_url):    
    lrModel, spark, sampled = logistic_model()
    #Extract test_url features
    test_df = pd.DataFrame(columns=['url', 'label'])
    test_df['url'] = test_url
    test_df['label'] = 0
    test_df_SPARK = spark.createDataFrame(test_df)
    sampled = sampled.union(test_df_SPARK)
    #Vectorisation
    dataset, pipelineFit = vectorize_data(sampled)
    #select the test data
    test_df_SPARK = spark.createDataFrame(dataset.toPandas().tail(1))
    
    lr_predictions = lrModel.transform(test_df_SPARK)
    y_pred = lr_predictions.select("prediction").toPandas()
    
    if (y_pred.prediction[0]==1.0):
        return "pass"
    else:
        return "fail" 


test_url = ["zvwqwnfguqcauhcmzjfskgimndynj.ru"]
integration_test(test_url)
    
    
